###Installation:
---
1. Clone the ABY git repository by running:

	git clone https://github.com/encryptogroup/ABY.git

2. Clone the fyp git repository by running:

	git clone https://why418@bitbucket.org/why418/fyp.git

3. Copy file from fyp folder to aby

	cp fyp/CMakeLists.txt ABY/src/examples/CMakeLists.txt
    
	cp -R fyp/project/ ABY/src/examples/

4. Modify ABY/src/abycore/ABY_utils/ABYconstants.h

	vi ABY/src/abycore/ABY_utils/ABYconstants.h
    
	Change "#define PRINT_PERFORMANCE_STATS 0" to "#define PRINT_PERFORMANCE_STATS 1"

5. Enter ABY directory

	cd ABY/

6. Create and enter the build directory:

	mkdir build && cd build

7. Complie the fyp project, the build executable will be in directory bin/

	cmake .. -DABY_BUILD_EXE=On
    
	make

###Run the program:
---

1. Parameter of the executable:

	-r role (0/1). 0 for server and 1 for client.
    
	-f function name(cmp/relu/max/normsign). The function name of client side must be same as the function name of server side in order to execute the program correctly.

2. Run the executable for server first (You can change cmp to relu/max/normsign to test other function):

	./project -r 0 -f cmp

3. Open a new terminal and run the executable for client (You can change cmp to relu/max/normsign to test other function but it must be same as the function of server side):

	./prject -r 1 -f cmp

4. The performance result of running function will be shown
