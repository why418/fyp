
#include "../../abycore/sharing/sharing.h"
#include "../../abycore/circuit/booleancircuits.h"
#include <ENCRYPTO_utils/crypto/crypto.h>
#include <ENCRYPTO_utils/parse_options.h>
//#include "../aes/common/aescircuit.h"

#include "../../abycore/aby/abyparty.h"
#include <random>
#include <cstring>
#include <iostream>

using namespace std;

// Helper Functions

template< class F, class... Args>
std::invoke_result_t<F, Args...> timing_invoke(string name, F&& f, Args&&... args) 
      noexcept(std::is_nothrow_invocable_v<F, Args...>)
{
    auto time_start = chrono::high_resolution_clock::now();
    std::invoke(std::forward<F>(f), std::forward<Args>(args)...);
    auto time_end = chrono::high_resolution_clock::now();
    cout << "Time for " + name +  ":  " << 
        chrono::duration_cast<chrono::microseconds>(time_end - time_start).count()  <<
        " microseconds" << endl;
}

class SimpleRandomGen {
public:
    SimpleRandomGen(int64_t seed_) : seed(seed_), x(seed_) {}

    int64_t gen_val();

    int64_t seed;
    int64_t x;
    const int64_t a = 1103515245;
    const int64_t c = 12345;
    const int64_t mod = static_cast<int64_t>(1) << 31;
};


class Randomness {
public:
    Randomness(int64_t seed_, bool is_simple_ = false) : 
        seed(seed_),
        is_simple(is_simple_),
        rd_simple(seed),
        rd_pseudo() {}

    inline int64_t gen_val() {
        return rd_pseudo();
        // return (is_simple ? rd_simple.gen_val() : rd_pseudo());
    }

    int64_t seed;
    bool is_simple;
    SimpleRandomGen rd_simple;
    random_device rd_pseudo;
};

// End of Helpler Functions

enum class FunctionName {
    CMP,
    RELU,
    NORMSIGN,
    MAX,
};

enum class GeneralRole {
    SERVER,
    CLIENT
};

int32_t read_test_options(int32_t* argcp, char*** argvp, e_role* role,std::string* func) {

	uint32_t int_role = 0;
	bool useffc = false;

	parsing_ctx options[] =
			{ { (void*) &int_role, T_NUM, "r", "Role: 0/1", true, false },{(void*) func, T_STR, "f", "funcname", true, false },};

	if (!parse_options(argcp, argvp, options,
			sizeof(options) / sizeof(parsing_ctx))) {
		print_usage(*argvp[0], options, sizeof(options) / sizeof(parsing_ctx));
		std::cout << "Exiting" << std::endl;
		exit(0);
	}

	assert(int_role < 2);
	*role = (e_role) int_role;

	//delete options;

	return 1;
}

int ABY_CMP(ABYParty* party,int role,std::vector<int>& input,std::vector<int>& output){ //CHeck if server>=client. haven't change to A sharing.

	uint32_t *s_input=new uint32_t[input.size()];
	uint32_t bitlen = 10;
	for(int i=0;i<input.size();i++){ //get uint_32t input by input vector
		s_input[i]=input.at(i);
	}
	e_sharing sharing = S_ARITH;
	uint32_t nvals=input.size();
	std::vector<Sharing*>& sharings = party->GetSharings();
	Circuit* ac = sharings[S_ARITH]->GetCircuitBuildRoutine();	
	Circuit* yc = sharings[S_YAO]->GetCircuitBuildRoutine();
	Circuit* bc = sharings[S_BOOL]->GetCircuitBuildRoutine();
	share *s_inputa, *s_inputb, *s_out,*s_res,*s_inputx,*s_inputy;
	uint32_t out_bitlen,out_nvals;
	uint32_t *out_vals=new uint32_t[input.size()];
	if(role == 0) {	
		s_inputa=ac->PutDummySIMDINGate(nvals,bitlen);
		s_inputb=ac->PutSIMDINGate(nvals,s_input,bitlen,SERVER);
	} else { //role == CLIENT
		s_inputa=ac->PutSIMDINGate(nvals,s_input,bitlen,CLIENT);
		s_inputb=ac->PutDummySIMDINGate(nvals,bitlen);
	}
	s_inputa=yc->PutA2YGate(s_inputa); 
	s_inputb=yc->PutA2YGate(s_inputb);
	uint32_t constant=1;
	s_inputx=yc->PutSIMDCONSGate(nvals,constant,bitlen);
	s_inputy=yc->PutADDGate(s_inputb,s_inputx);
	s_res=yc->PutGTGate(s_inputy,s_inputa);

	s_res=bc->PutY2BGate(s_res);
	s_res=ac->PutB2AGate(s_res);
	s_res=ac->PutSharedOUTGate(s_res);
	party->ExecCircuit();



	s_res->get_clear_value_vec(&out_vals,&out_bitlen,&out_nvals);

if(out_nvals!=input.size()){ //Number of output should be same as number of input.
		return 1;				
	}

	for(int i=0;i<input.size();i++){ //set value of output vector 
		output.at(i)=out_vals[i];

	}

	return 0; //If the bitlen and nvals is correct,return 0.
}

int ABY_RELU(ABYParty* party,int role,std::vector<int>& input,std::vector<int>& output){ 
	uint32_t *s_input=new uint32_t[input.size()];
	uint32_t bitlen = 10;
	for(int i=0;i<input.size();i++){ //get uint_32t input by input vector
		s_input[i]=input.at(i);
	}
	e_sharing sharing = S_ARITH;
	uint32_t nvals=input.size();
	std::vector<Sharing*>& sharings = party->GetSharings();
	Circuit* ac = sharings[S_ARITH]->GetCircuitBuildRoutine();	
	Circuit* yc = sharings[S_YAO]->GetCircuitBuildRoutine();
	Circuit* bc = sharings[S_BOOL]->GetCircuitBuildRoutine();
	share *s_inputa, *s_inputb, *s_out,*s_res,*s_inputx,*s_inputy,*s_sel;
	uint32_t out_bitlen,out_nvals;
	uint32_t *out_vals=new uint32_t[input.size()];
	s_inputa=ac->PutSharedSIMDINGate(nvals,s_input,bitlen);
	s_inputa=yc->PutA2YGate(s_inputa);
	uint32_t constant=0;
	s_inputb=yc->PutSIMDCONSGate(nvals,constant,bitlen);
	constant=2048;
	s_inputx=yc->PutSIMDCONSGate(nvals,constant,bitlen);
	s_inputy=yc->PutADDGate(s_inputa,s_inputx);
	s_sel=yc->PutGTGate(s_inputy,s_inputx);
	s_res=yc->PutMUXGate(s_inputa,s_inputb,s_sel);
	s_res=bc->PutY2BGate(s_res);
	s_res=ac->PutB2AGate(s_res);
	s_res=ac->PutSharedOUTGate(s_res);
	//timing_invoke("running CIRCUIT", [&]() {
	party->ExecCircuit();
   // });


	s_res->get_clear_value_vec(&out_vals,&out_bitlen,&out_nvals);
if(out_nvals!=input.size()){ //Number of output should be same as number of input.
		return 1;				
	}


	for(int i=0;i<input.size();i++){ //set value of output vector 
		output.at(i)=out_vals[i];

	}

	return 0;
}
int ABY_MAX(ABYParty* party,int role,std::vector<int>& input,std::vector<int>& input2,std::vector<int>& output){ //CHeck if server>=client. haven't change to A sharing.
	uint32_t *s_input=new uint32_t[input.size()];
	uint32_t *s_input2=new uint32_t[input.size()];
	uint32_t bitlen = 10;
	for(int i=0;i<input.size();i++){ //get uint_32t input by input vector
		s_input[i]=input.at(i);
		s_input2[i]=input2.at(i);
	}
	e_sharing sharing = S_ARITH;
	uint32_t nvals=input.size();
	std::vector<Sharing*>& sharings = party->GetSharings();
	Circuit* ac = sharings[S_ARITH]->GetCircuitBuildRoutine();	
	Circuit* yc = sharings[S_YAO]->GetCircuitBuildRoutine();
	Circuit* bc = sharings[S_BOOL]->GetCircuitBuildRoutine();
	share *s_inputa, *s_inputb,*s_inputc,*s_inputd, *s_out,*s_res,*s_inputx,*s_inputy,*s_sel;
	uint32_t out_bitlen,out_nvals;
	uint32_t *out_vals=new uint32_t[input.size()];
	s_inputa=ac->PutSharedSIMDINGate(nvals,s_input,bitlen);
	s_inputb=ac->PutSharedSIMDINGate(nvals,s_input2,bitlen);
	s_inputa=yc->PutA2YGate(s_inputa); 
	s_inputb=yc->PutA2YGate(s_inputb);
	s_sel=yc->PutGTGate(s_inputb,s_inputa);
	s_res=yc->PutMUXGate(s_inputb,s_inputa,s_sel);
	s_res=bc->PutY2BGate(s_res);
	s_res=ac->PutB2AGate(s_res);
	s_res=ac->PutSharedOUTGate(s_res);
//	timing_invoke("running CIRCUIT", [&]() {
	party->ExecCircuit();
  //  });

	s_res->get_clear_value_vec(&out_vals,&out_bitlen,&out_nvals);
if(out_nvals!=input.size()){ //Number of output should be same as number of input.
		return 1;				
	}

	for(int i=0;i<input.size();i++){ //set value of output vector 
		output.at(i)=out_vals[i];

	}
	return 0;
}
int ABY_Normsign(ABYParty* party,int role,std::vector<int>& input,std::vector<int>& input2,std::vector<int>& output){ //CHeck if server>=client. haven't change to A sharing.
	uint32_t *s_input=new uint32_t[input.size()];
	uint32_t *s_input2=new uint32_t[input.size()];
	uint32_t bitlen = 10;
	for(int i=0;i<input.size();i++){ //get uint_32t input by input vector
		s_input[i]=input.at(i);
		s_input2[i]=input2.at(i);
	}
	e_sharing sharing = S_ARITH;
	uint32_t nvals=input.size();
	std::vector<Sharing*>& sharings = party->GetSharings();
	Circuit* ac = sharings[S_ARITH]->GetCircuitBuildRoutine();	
	Circuit* yc = sharings[S_YAO]->GetCircuitBuildRoutine();
	Circuit* bc = sharings[S_BOOL]->GetCircuitBuildRoutine();
	share *s_inputa, *s_inputb,*s_inputc,*s_inputd, *s_out,*s_res,*s_inputx,*s_inputy,*s_sel;
	uint32_t out_bitlen,out_nvals;
	uint32_t *out_vals=new uint32_t[input.size()];
	s_inputa=ac->PutSharedSIMDINGate(nvals,s_input,bitlen);
	s_inputb=ac->PutSharedSIMDINGate(nvals,s_input2,bitlen);
	s_inputa=yc->PutA2YGate(s_inputa); 
	s_inputb=yc->PutA2YGate(s_inputb);
	s_sel=yc->PutGTGate(s_inputb,s_inputa);

	uint32_t constant=1;
	s_inputx=yc->PutSIMDCONSGate(nvals,constant,bitlen);
	constant=-1;
	s_inputy=yc->PutSIMDCONSGate(nvals,constant,bitlen);

	s_res=yc->PutMUXGate(s_inputy,s_inputx,s_sel);
	s_res=bc->PutY2BGate(s_res);
	s_res=ac->PutB2AGate(s_res);
	s_res=ac->PutSharedOUTGate(s_res);
//	timing_invoke("running CIRCUIT", [&]() {
	party->ExecCircuit();
  //  });

	s_res->get_clear_value_vec(&out_vals,&out_bitlen,&out_nvals);
if(out_nvals!=input.size()){ //Number of output should be same as number of input.
		return 1;				
	}

	for(int i=0;i<input.size();i++){ //set value of output vector 
		output.at(i)=out_vals[i];

	}
	return 0;
}
int ABY_computation(ABYParty* party,const GeneralRole role, const FunctionName func_name, const vector<int>& input_1, const vector<int>& input_2, vector<int>& output){

return 0;

}
int main(int argc, char** argv) {

	e_role role;
	uint32_t bitlen = 10, secparam = 128, nthreads = 4;
	uint32_t port = 7766;
	std::string address = "127.0.0.1";
	std::string funcname = "cmp";
	int32_t test_op = -1;

	read_test_options(&argc, &argv, &role,&funcname);
	Randomness t(31,true);

	seclvl seclvl = get_sec_lvl(secparam);
	e_mt_gen_alg mt_alg = MT_OT;
	ABYParty* party = new ABYParty(role, address, port, seclvl, bitlen, nthreads,mt_alg);

	std::vector<int> input(12288);

	std::vector<int> input2(input.size());

	std::vector<int> output(input.size()); //create output vector by size of input
    

	int exec_res;

	//exec_res = ABY_CMP(party,(int) role,input,output);

	if(funcname=="cmp"){
		for(int i=0;i<input.size();i++){  //generate input of server
			input.at(i)=(t.gen_val())%2048;
		}
		if(role==CLIENT){  //generate input of client
			for(int i=0;i<input.size();i++){  
				input.at(i)=(t.gen_val())%2048;
			}
		}
		exec_res = ABY_CMP(party,(int) role,input,output);

	}
	else if(funcname=="relu"){
		for(int i=0;i<input.size();i++){  //generate input of server
			input.at(i)=(t.gen_val())%2048;
		}
		if(role==CLIENT){  //generate input of client
			for(int i=0;i<input.size();i++){  
				input.at(i)=-(t.gen_val())%2048;
			}
		}
		exec_res = ABY_RELU(party,(int) role,input,output);

	}
	else if(funcname=="max"){

		for(int i=0;i<input.size();i++){  //generate input of server
			input.at(i)=(t.gen_val())%1024;
		}
		if(role==CLIENT){  //generate input of client
			for(int i=0;i<input.size();i++){  
				input.at(i)=(t.gen_val())%1024;
			}
		}
		for(int i=0;i<input.size();i++){  //generate input of server
			input2.at(i)=(t.gen_val())%1024;
		}
		if(role==CLIENT){  //generate input of client
			for(int i=0;i<input.size();i++){  
				input2.at(i)=(t.gen_val())%1024;
			}
		}
		exec_res = ABY_MAX(party,(int) role,input,input2,output);
	}
	else if(funcname=="normsign"){
		for(int i=0;i<input.size();i++){  //generate input of server
			input.at(i)=(t.gen_val())%1024;
		}
		if(role==CLIENT){  //generate input of client
			for(int i=0;i<input.size();i++){  
				input.at(i)=(t.gen_val())%1024;
			}
		}
		for(int i=0;i<input.size();i++){  //generate input of server
			input2.at(i)=(t.gen_val())%1024;
		}
		if(role==CLIENT){  //generate input of client
			for(int i=0;i<input.size();i++){  
				input2.at(i)=(t.gen_val())%1024;
			}
		}
		exec_res = ABY_Normsign(party,(int) role,input,input2,output);
	}



/*
	timing_invoke("cout", [&]() {
ABY_computation(party,Grole,func_name,input,input2,output);
    });

*/
/*
	if(role==SERVER){
		for(int i=0;i<input.size();i++){
			std::cout<<"S_O"<<i<<":"<<output[i]<<std::endl;
			std::cout<<"S_1_In"<<i<<":"<<input[i]<<std::endl;
			std::cout<<"S_2_In"<<i<<":"<<input2[i]<<std::endl;
		}
	}
	else{
		for(int i=0;i<input.size();i++){
			std::cout<<"C_O"<<i<<":"<<output[i]<<std::endl;
			std::cout<<"C_1_In"<<i<<":"<<input[i]<<std::endl;
			std::cout<<"C_2_In"<<i<<":"<<input2[i]<<std::endl;
		}
	}
*/
	delete party;

	return 0;
}

